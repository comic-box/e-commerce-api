val Http4sVersion = "0.23.20"
val MunitVersion = "0.7.29"
val LogbackVersion = "1.4.8"
val MunitCatsEffectVersion = "1.0.7"

lazy val root = (project in file("."))
  .settings(
    organization := "com.it.unibo",
    name := "scala-api-pps",
    version := "0.0.1-SNAPSHOT",
    scalaVersion := "3.3.0",
    libraryDependencies ++= Seq(
      "org.http4s"      %% "http4s-ember-server" % Http4sVersion,
      "org.http4s"      %% "http4s-ember-client" % Http4sVersion,
      "org.http4s"      %% "http4s-circe"        % Http4sVersion,
      "org.http4s"      %% "http4s-dsl"          % Http4sVersion,
      "org.scalameta"   %% "munit"               % MunitVersion           % Test,
      "org.typelevel"   %% "munit-cats-effect-3" % MunitCatsEffectVersion % Test,
      "ch.qos.logback"  %  "logback-classic"     % LogbackVersion,
      "org.typelevel" %% "cats-effect" % "3.5.1",
       "co.fs2" %% "fs2-core" % "3.1.6",

// MongoDb connector that uses cats/effect
      // https://dzone.com/articles/scala-mongodb-and-cats-effect
      "io.github.kirill5k" %% "mongo4cats-core" % "0.3.0",
      "io.github.kirill5k" %% "mongo4cats-circe" % "0.3.0",
      //dependency ???
      "org.fusesource.jansi" % "jansi" % "1.18",
      "io.circe" %% "circe-core" % "0.14.1",
      "io.circe" %% "circe-generic" % "0.14.1",
      "io.circe" %% "circe-literal" % "0.14.5",
      //https://www.optics.dev/Monocle/
      "dev.optics" %% "monocle-core" % "3.1.0",
      "dev.optics" %% "monocle-macro" % "3.1.0"

      //      "org.mongodb.scala" %% "mongo-scala-driver" % "2.9.0"
      // "org.mongodb.scala" %% "mongo-scala-driver" % "2.9.0"
    //  "org.mongodb.scala" %% "mongo-scala-driver" % "2.13",
    //  "org.mongodb.scala" %% "mongo-circe" % "4.3.1",

    ),
    testFrameworks += new TestFramework("munit.Framework")
  )

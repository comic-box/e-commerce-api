package com.it.unibo.scalaapipps

import scala.io.Source
object EnvLoader:
  def loadEnv(filename: String): Map[String, String] = {
    val envFile = Source.fromFile(filename)
    val envMap = envFile.getLines().map { line =>
      val Array(key, value) = line.split('=')
      key -> value
    }.toMap
    envFile.close()
    envMap
  }
  
  def getKey(key :String) : String =
    val vars = EnvLoader.loadEnv(".env")
    vars.get(key).getOrElse(throw new RuntimeException(s"${key} not defined in .env"))


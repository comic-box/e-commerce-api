package com.it.unibo.scalaapipps.routes

import cats.effect.unsafe.implicits.global
import cats.effect.*
import cats.syntax.all.*
import com.it.unibo.scalaapipps.mongo.schemas.*
import com.it.unibo.scalaapipps.mongo.{Repository, RepositoryTrait}
import io.circe.Encoder.encodeString
import io.circe.Json
import io.circe.generic.*
import io.circe.generic.semiauto.deriveDecoder
import io.circe.parser.*
import org.bson.json.JsonParseException
import org.http4s
import org.http4s.circe.jsonOf
import org.http4s.dsl.{Http4sDsl, request}
import org.http4s.{EntityDecoder, HttpRoutes, Response, Status}

import scala.::
import scala.concurrent.Await
import scala.concurrent.duration.Duration

//val dsl = new Http4sDsl[IO] {}
//import dsl.*
import io.circe.generic.auto.*
import io.circe.syntax.*
import org.http4s.*
import org.http4s.circe.CirceEntityDecoder.*
import org.http4s.dsl.Http4sDsl.*
import org.http4s.dsl.io.*
import org.http4s.implicits.*

// import com.it.unibo.scalaapipps.mongo.Repository.given_EntityEncoder_IO_Json
// import org.http4s.circe.jsonEncoder
import com.it.unibo.scalaapipps.mongo.DatabaseConnection
import io.circe.JsonObject
import mongo4cats.bson.ObjectId
import monocle.function.Each.each
import monocle.macros.GenLens
import monocle.syntax.all.*
import org.bson.Document
import org.http4s.circe.CirceEntityCodec.circeEntityEncoder
import org.http4s.circe.CirceEntityEncoder.circeEntityEncoder

import com.it.unibo.scalaapipps.services.CartService._

object CartRoutes:
  given DatabaseConnection: DatabaseConnection = new DatabaseConnection()


  /** CRUD : Read, get from the cart from the customer passed as URL
    *
    * @param carts
    * @tparam F
    * @return
    * response with cart of customer as JSON
    */
  def getCartController[F[_] : Sync](carts: Ref[IO, List[Cart]]): HttpRoutes[IO] =
    HttpRoutes.of[IO]:
      case GET -> Root / "cart" / customerID =>
//        val cart = getCartForUserSync(customerID, carts).unsafeRunSync()
        IO(Response[IO](Status.Ok).withEntity(getUserCart(customerID, carts.get.unsafeRunSync())))

  /** Computes the total cost of a current user cart
    *
    * @param carts
    * @tparam F
    * @return
    */
  def getTotalController[F[_] : Sync](carts: Ref[IO, List[Cart]]): HttpRoutes[IO] =
    HttpRoutes.of[IO]:
      case GET -> Root / "cart" / "total" / customerID => computeTotal(customerID, carts)

  /** CRUD update/create (upsert), add a product to the cart of the user selected by tue URL
    *
    * @param R
    * repository
    * @param carts
    * Ref map
    * @tparam F
    * BODY : { productID : String, volumeNumber : Integer, volumeQty: Integer }
    * @return
    */
  def addProductController[F[_] : Sync](
                               repository: RepositoryTrait[IO],
                               carts: Ref[IO, List[Cart]]
                             ): HttpRoutes[IO] =
    HttpRoutes.of[IO]:
      case req@POST -> Root / "cart" / customerID => addProduct(req, repository, carts, customerID)



  /** CRUD delete, removes the specified product from the cart of the customer specified by the URL
    *
    * @param carts
    * @tparam F
    * @return
    *
    * BODY : {"productId" : String }
    */
  def removeCartProductController[F[_] : Sync](
                                      repository: RepositoryTrait[IO],
                                      carts: Ref[IO, List[Cart]],
                                      userAction: Boolean = true
                                    ): HttpRoutes[IO] =
    HttpRoutes.of[IO]:
      case req@DELETE -> Root / "cart" / customerId => deleteCartProd(req, customerId, repository, carts, userAction)

package com.it.unibo.scalaapipps.routes

import cats.data.Validated
import cats.effect.unsafe.implicits.global
import cats.effect.{IO, Ref, Sync}
import com.it.unibo.scalaapipps.dataValidation.*
import com.it.unibo.scalaapipps.mongo.schemas.Cart
import io.circe.Json
import io.circe.generic.*
import io.circe.parser.*
import org.bson.json.*
import org.http4s.dsl.Http4sDsl
import org.http4s.{HttpRoutes, Response, Status}

val dsl = new Http4sDsl[IO] {}
import dsl.*
import io.circe.*
import io.circe.generic.auto.*
import io.circe.generic.semiauto.*
import io.circe.syntax.*
import com.it.unibo.scalaapipps.services.ValidateService.validateOperation


def validateDataController[A, B](path : String, base : A): HttpRoutes[IO] =
  HttpRoutes.of[IO]:
    case req@ POST -> Root / "validate" / path => validateOperation(req, base)

object CheckoutRoutes:
  def validateUser[F[_] : Sync](): HttpRoutes[IO] =
    validateDataController[UserDto, UserValidator](UserDto.route, UserDto())

  def validateCC[F[_] : Sync](): HttpRoutes[IO] =
    validateDataController[CreditCard, CreditCardValidator](CreditCard.route, CreditCard())

  def validateAddress[F[_] : Sync](): HttpRoutes[IO] =
    validateDataController[Address, AddressValidator](Address.route, Address())


package com.it.unibo.scalaapipps.routes

import cats.effect.*
import cats.effect.unsafe.implicits.global
import cats.syntax.all.*
import com.it.unibo.scalaapipps.mongo.Repository.Repository
import com.it.unibo.scalaapipps.mongo.schemas.*
import com.it.unibo.scalaapipps.mongo.{DatabaseConnection, Repository, RepositoryTrait}
import com.it.unibo.scalaapipps.routes.CartRoutes
import io.circe.Encoder.encodeString
import io.circe.generic.auto.*
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.literal.json
import io.circe.syntax.*
import io.circe.{Decoder, Encoder, Json, JsonObject}
import mongo4cats.bson.ObjectId
import monocle.function.Each.each
import monocle.macros.GenLens
import monocle.syntax.all.*
import org.bson.Document
import org.http4s
import org.http4s.circe.CirceEntityCodec.circeEntityEncoder
import org.http4s.circe.CirceEntityDecoder.*
import org.http4s.circe.CirceEntityEncoder.circeEntityEncoder
import org.http4s.circe.jsonOf
import org.http4s.dsl.Http4sDsl.*
import org.http4s.dsl.io.*
import org.http4s.dsl.{Http4sDsl, request}
import org.http4s.implicits.*
import org.http4s.*

import scala.::
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import com.it.unibo.scalaapipps.services.OrderService.*


object OrderRoutes:
  given DatabaseConnection: DatabaseConnection = new DatabaseConnection()

  def processOrderController[F[_] : Sync](
                                 rep: RepositoryTrait[IO],
                                 carts: Ref[IO, List[Cart]],
                                 orders: Ref[IO, Map[String, List[Order]]]
                               ): HttpRoutes[IO] =
    HttpRoutes.of[IO] :
      case POST -> Root / "order" / userID => processOrder(rep, carts, orders, userID)

  def getOrderController[F[_] : Sync](orders: Ref[IO, Map[String, List[Order]]]): HttpRoutes[IO] =
    HttpRoutes.of[IO]:
      case GET -> Root / "order" / customerId => getCustomerOrder(orders, customerId)

  def removeOrderController[F[_] : Sync](orders: Ref[IO, Map[String, List[Order]]]): HttpRoutes[IO] =

  /** utilissimo monocle
    */
    HttpRoutes.of[IO] :
      case DELETE -> Root / "order" / customerId => deleteOrder(orders, customerId)
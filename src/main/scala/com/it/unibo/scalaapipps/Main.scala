package com.it.unibo.scalaapipps

import cats.effect.{Async, Deferred, ExitCode, IO, IOApp}
import com.it.unibo.scalaapipps.mongo.{DatabaseConnection, Repository}
import org.bson.Document
import cats.effect.unsafe.implicits.global

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

object Main extends IOApp.Simple:
  given DatabaseConnection: DatabaseConnection = new DatabaseConnection()
  val run = ScalaapippsServer.run().onCancel(Repository.impl[IO](EnvLoader.getKey("MONGO_DB_NAME")).stopService())

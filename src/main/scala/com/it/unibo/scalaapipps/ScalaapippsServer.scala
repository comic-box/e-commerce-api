package com.it.unibo.scalaapipps

import cats.effect.{Async, IO, Ref}
import cats.syntax.all.*
import com.comcast.ip4s.*
import fs2.io.net.Network
import org.http4s.ember.client.EmberClientBuilder
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.implicits.*
import org.http4s.server.middleware.{CORS, Logger}
import com.it.unibo.scalaapipps.mongo.*
import com.it.unibo.scalaapipps.mongo.schemas.{Cart, Order}
import com.it.unibo.scalaapipps.routes.{CartRoutes, CheckoutRoutes, OrderRoutes}
import org.http4s.HttpApp

object ScalaapippsServer:
  def run(): IO[Unit] =
    EmberServerBuilder
      .default[IO]
      .withHost(ipv4"0.0.0.0")
      .withPort(port"1040")
      .withHttpApp(finalHttpApp)
      .build
      .useForever

  private def finalHttpApp: HttpApp[IO] =
    val repo = Repository.impl[IO](EnvLoader.getKey("MONGO_DB_NAME"))
    val userCarts: Ref[IO, List[Cart]] = Ref.unsafe[IO, List[Cart]](List.empty)
    val serverOrders: Ref[IO, Map[String, List[Order]]] = Ref.unsafe[IO, Map[String, List[Order]]](Map())
    val httpApp = (
      CORS.policy.withAllowOriginAll(CartRoutes.addProductController[IO](repo, userCarts)) <+>
        CORS.policy.withAllowOriginAll(CartRoutes.getCartController[IO](userCarts)) <+>
        CORS.policy.withAllowOriginAll(CartRoutes.getTotalController[IO](userCarts)) <+>
        CORS.policy.withAllowOriginAll(CartRoutes.removeCartProductController[IO](repo, userCarts)) <+>
        CORS.policy.withAllowOriginAll(OrderRoutes.processOrderController[IO](repo, userCarts, serverOrders)) <+>
        CORS.policy.withAllowOriginAll(OrderRoutes.getOrderController[IO](serverOrders)) <+>
        CORS.policy.withAllowOriginAll(OrderRoutes.removeOrderController[IO](serverOrders)) <+>
        CORS.policy.withAllowOriginAll(CheckoutRoutes.validateUser[IO]()) <+>
        CORS.policy.withAllowOriginAll(CheckoutRoutes.validateAddress[IO]()) <+>
        CORS.policy.withAllowOriginAll(CheckoutRoutes.validateCC[IO]())
      ).orNotFound
    Logger.httpApp(true, true)(httpApp)


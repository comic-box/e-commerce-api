package com.it.unibo.scalaapipps.services

import cats.effect.{IO, Ref, Sync}
import org.http4s.{HttpRoutes, Method, Request, Response, Status}
import org.http4s.circe.CirceEntityEncoder.circeEntityEncoder
import com.it.unibo.scalaapipps.mongo.{DatabaseConnection, RepositoryTrait}
import com.it.unibo.scalaapipps.mongo.schemas.{Cart, CartProduct, CartProductID, Manga, Order, OrderItem, OrderProduct, ReservedVolume, VolumeToOrder}
import com.it.unibo.scalaapipps.routes.CartRoutes
import org.http4s.implicits.uri
import io.circe.generic.*
import io.circe.literal.json
import io.circe.generic.auto.*
import io.circe.syntax.*
import io.circe.parser.*
import monocle.function.Each.each
import monocle.macros.GenLens
import monocle.syntax.all.*
import cats.effect.unsafe.implicits.global
import com.it.unibo.scalaapipps.mongo.schemas.OrderStatus.{Shipped, ShippedPartially}
import io.circe.Json
import mongo4cats.bson.ObjectId

object OrderService:

  given DatabaseConnection: DatabaseConnection = new DatabaseConnection()

  /**
    * Functionality to empty the cart after the order has been evaluted
    * @param repository
    * @param carts
    * @param cartProdID
    * @param customerID
    * @tparam F
    * @return Response[IO]
    */
  private def emptyCart[F[_] : Sync](
                              repository: RepositoryTrait[IO],
                              carts: Ref[IO, List[Cart]],
                              cartProdID: CartProductID,
                              customerID: String
                            ) =
    val removeProduct = Request[IO](Method.DELETE, uri"/cart"./(customerID))
    CartRoutes
      .removeCartProductController(repository, carts, false)
      .orNotFound(removeProduct.withEntity(cartProdID))
      .unsafeRunSync()

  /**
    * Internal function used to write into the orders Ref
    * @param userID
    * @param orders
    * @param fakeOrder
    * @tparam F
    */
  private def addOrderToRef[F[_] : Sync](
                                          userID: String,
                                          orders: Ref[IO, Map[String, List[Order]]],
                                          fakeOrder: Order
                                        ): Unit =
    orders
      .update { orderMap =>
        orderMap.get(userID) match
          case Some(_) => orderMap.focus().at(userID).modify((x: Option[List[Order]]) => Some(fakeOrder :: x.get))
          // Monocle if exists a value replace it if not creates
          case None => orderMap.focus().at(userID).replace(Some(fakeOrder :: Nil))
      }
      .unsafeRunSync()

  /**
    * Process the cart from a specific user and translate it into an order
    * that will be eventually evaluted from a deliver agency
    * @param rep
    * @param carts
    * @param orders
    * @param userID
    * @tparam F
    * @return IO[Response[IO]]
    */
  def processOrder[F[_] : Sync](rep: RepositoryTrait[IO], carts: Ref[IO, List[Cart]], orders: Ref[IO, Map[String, List[Order]]], userID: String) =
    try {
      val prods = carts.get.map(_.find(_.customerID == userID).get).unsafeRunSync()
      prods.cartProducts.foreach( cartProd =>
        // Don't forget to empty the cart
        emptyCart(rep, carts, CartProductID(cartProd.productID), userID)
        val product = rep.getManga(cartProd.productID).unsafeRunSync().get
        cartProd.volumes.forall(vol => rep.checkDispo(cartProd.productID.toString, vol).unsafeRunSync()) match
          case true => allCartProductsAvailable(orders, userID, cartProd, rep, product)
          case _ => someCartProductIsNotAvailable(orders, userID, cartProd, rep ,product)
        )
      IO(Response[IO](Status.Ok).withEntity(Json.True))
    } catch {
      case e: NoSuchElementException => IO(Response[IO](Status.NotFound).withEntity(Json.Null))
    }

  /**
    * Function utility used to process an order that has some item that need an action, not completely available in the
    * warehouse
    * @param orders
    * @param userID
    * @param cartProd
    * @param rep
    * @param product
    * @tparam F
    */
  private def someCartProductIsNotAvailable[F[_] : Sync](orders: Ref[IO, Map[String, List[Order]]], userID: String, cartProd: CartProduct, rep: RepositoryTrait[IO], product : Manga): Unit = {
    val orderResponses = cartProd.volumes.map(vol => {
      val currentVolume = product.volumes.find(_.number == vol.number).get
      currentVolume.quantity - vol.quantity match
        case dispo if dispo >= 0 =>
          rep
            .updateMangaReservation(cartProd.productID, vol.number, currentVolume.reserved - vol.quantity)
            .unsafeRunSync()
          rep.updateMangaQtyWithID(cartProd.productID, vol.number, dispo).unsafeRunSync()
          OrderProduct(cartProd.productID, vol.number, false, currentVolume.reserved, dispo)
        case _ =>
          rep
            .updateMangaReservation(
              cartProd.productID,
              vol.number,
              currentVolume.reserved - vol.quantity
            )
            .unsafeRunSync() // product out of stock
          rep.updateMangaQtyWithID(cartProd.productID, vol.number, 0).unsafeRunSync()
          OrderProduct(cartProd.productID, vol.number, true, vol.quantity, currentVolume.quantity)
    })
    addOrderToRef(
      userID,
      orders,
      Order(
        {
          orderResponses.find(_.modified == true) match
            case Some(_) => ShippedPartially
            case _ => Shipped
        },
        new ObjectId().toString,
        orderResponses
          .map(modifiedOrder =>
            modifiedOrder.modified match
              case true => OrderItem(
                product.searchID,
                Some(VolumeToOrder(modifiedOrder.number, modifiedOrder.newDispo)),
                product.price,
                Some(modifiedOrder.toString)
              )
              case false => OrderItem(
                product.searchID,
                None,
                product.price,
                Some(modifiedOrder.toString)
              )
          )
      )
    )
  }

  /**
    *
    * Function utility used to process an order that has all items available in the warehouse
    * @param orders
    * @param userID
    * @param cartProd
    * @param rep
    * @param product
    * @tparam F
    * @return
    */
  private def allCartProductsAvailable[F[_] : Sync](orders: Ref[IO, Map[String, List[Order]]], userID: String, cartProd: CartProduct, rep: RepositoryTrait[IO], product : Manga) = {
    addOrderToRef(
      userID,
      orders,
      Order(
        Shipped,
        new ObjectId().toString,
        cartProd.volumes.map(vol => OrderItem(product.searchID, Some(vol), product.price))
      )
    )
    cartProd.volumes.map(vol => {
      val currentVolume = product.volumes.find(_.number == vol.number).get
      rep
        .updateMangaReservation(cartProd.productID, vol.number, currentVolume.reserved - vol.quantity)
        .unsafeRunSync()
      rep
        .updateMangaQtyWithID(cartProd.productID, vol.number, currentVolume.quantity - vol.quantity)
        .unsafeRunSync()
      OrderProduct(cartProd.productID, vol.number, false, vol.quantity, currentVolume.quantity - vol.quantity)
    })
  }

  /**
    * Return the orders that a certain user has made
    *
    * @param orders
    * @param customerId
    * @tparam F
    * @return IO[Response[IO]]
    */
  def getCustomerOrder[F[_] : Sync](orders: Ref[IO, Map[String, List[Order]]], customerId: String) =
    val result = orders.get.unsafeRunSync()
    val response: Response[IO] = Response[IO](Status.Ok)
      .withEntity(
        result.get(customerId) match
          case Some(orders) => for {
            order <- orders
            prices = order.items.map(prod => prod.item.get.quantity * prod.price)
          } yield Order(order.status, order.orderID, order.items, prices.sum.toInt)
          case None => List.empty[Order]
      )
    IO.pure(response)

  /**
    * Functionality that can erase an order from the history
    * @param orders
    * @param customerId
    * @tparam F
    * @return IO[Response[IO]]
    */
  def deleteOrder[F[_] : Sync](orders: Ref[IO, Map[String, List[Order]]], customerId: String) =
    val result = orders
      .updateAndGet { orders =>
        orders.focus().at(customerId).replace(Some(Nil))
      }.unsafeRunSync()
    IO.pure(Response[IO](Status.Ok).withEntity(result.get(customerId).get.asJson))

package com.it.unibo.scalaapipps.services

import cats.effect.{IO, Ref, Sync}
import com.it.unibo.scalaapipps.mongo.schemas.{AddCartProduct, Cart, CartProduct, CartProductID, MangaNotFound, ReservedVolume, Total}
import org.http4s.{HttpRoutes, Request, Response, Status}
import cats.effect.unsafe.implicits.global
import com.it.unibo.scalaapipps.mongo.{DatabaseConnection, RepositoryTrait}
import org.http4s.circe.CirceEntityEncoder.circeEntityEncoder
import io.circe.generic.*
import io.circe.literal.json
import io.circe.generic.auto.*
import io.circe.syntax.*
import io.circe.parser.*
import org.bson.json.JsonParseException
import monocle.function.Each.each
import monocle.macros.GenLens
import monocle.syntax.all.*
import cats.syntax.all.{catsSyntaxTuple2Parallel, toFoldableOps}
import io.circe.Json
object CartService:

  given DatabaseConnection: DatabaseConnection = new DatabaseConnection()
  /** Gets user Cart, it's not necessary to throw or consider the case in which user cant have a cart, by deafult users
    * have an empty Cart
    *
    * @param customerId
    * @param carts
    * @tparam F
    * @return Cart
    */
  def getUserCart[F[_] : Sync](customerId: String, carts: List[Cart]) =
    carts.find(_.customerID == customerId) match
      case Some(cart) => cart
      case None => Cart(customerId, List.empty[CartProduct])


  /**
    * Compute the total cost of the cart by summing the prices
    * @param customerID
    * @param carts
    * @tparam F
    * @return IO[Response[IO]]
    */
  def computeTotal[F[_] : Sync](customerID: String, carts: Ref[IO, List[Cart]]) =
    val result = this.getUserCart(customerID, carts.get.unsafeRunSync())
    val costProducts = for {
      cartProduct <- result.cartProducts
      totalVolumeQuantity = cartProduct.volumes.map(_.quantity).sum
    } yield totalVolumeQuantity * cartProduct.price
    IO(Response[IO](Status.Ok).withEntity(json"""{"total" : ${costProducts.sum}}"""))


  /**
    * Utility used to parse the req body as an IO[string] and so as a string
    * @param req
    * @tparam F
    * @return String
    */
  private def parseRequestBody[F[_] : Sync](req: Request[IO]) =
    req.body
      .through(fs2.text.utf8.decode)
      .compile
      .string
      .unsafeRunSync()

  /**
    * Function to manage the insertion of a product in a cart that has already some other volume
    * here is distinguished the case in which the incoming volume is already in the cart, so the
    * quantity need to be incremented or the viceversa so the list is incremented
    *
    * @param req
    * @param repository
    * @param carts
    * @param customerID
    * @tparam F
    * @return IO[Response[IO]]
    */
  def addProduct[F[_] : Sync](req : Request[IO], repository: RepositoryTrait[IO], carts: Ref[IO, List[Cart]], customerID : String) =
    val encodedBody: String = parseRequestBody(req)

    val cartBody: AddCartProduct = decode[AddCartProduct](encodedBody) match {
      case Right(value) => value
      case Left(error) => throw JsonParseException(s"Malformed Body ${error}")
    }
    try {
      val decodedBody = CartProduct(cartBody.productID, repository.getManga(cartBody.productID).unsafeRunSync().get.price, cartBody.volume :: Nil)
      println(decodedBody)
      if (!repository.checkDispo(cartBody.productID, cartBody.volume).unsafeRunSync()) throw new NoSuchElementException()
      val updateAndGetRefIO = carts.updateAndGet { cartList =>
        cartList.find(_.customerID == customerID) match
          case Some(cart) =>
            val customerKey = cartList.map(_.customerID).indexOf(customerID)
            cartList.get(customerKey).get.cartProducts match
              case ::(head, next) => manageCartProductQueue(customerID, cartBody, decodedBody, cartList, cart, customerKey)
              case Nil =>
                cartList
                  .focus(_.index(customerKey).cartProducts)
                  .modify(_ =>
                    List(
                      decodedBody
                    )
                  )
          case None =>
            Cart(
              customerID,
              decodedBody :: Nil
            ) :: cartList
      }
      val incrementMangaReservationIO = repository.incrementMangaReservationWithID(
        decodedBody.productID,
        cartBody.volume.number
      )

      val combinedIO = (updateAndGetRefIO, incrementMangaReservationIO).parTupled
      combinedIO.attempt.flatMap {
        case Right((updateAndGetRefResult, incrementResult)) => IO.pure(Response[IO](Status.Ok)
          .withEntity(getUserCart(customerID, updateAndGetRefResult)))
        case Left(err) => IO(Response[IO](Status.InternalServerError).withEntity(Json.False))
      }

    } catch {
      case err: NoSuchElementException => IO(Response[IO](Status.NotFound).withEntity(err.toString))
      case err: MangaNotFound => IO.pure(Response[IO](Status.NotFound).withEntity(err.toString))
    }

    /**
      * Focus on the cart product addition into a queque
      * @param customerId
      * @param cartBody
      * @param decodedBody
      * @param cartList
      * @param cart
      * @param customerKey
      * @tparam F
      * @return List[Cart]
      */
  private def manageCartProductQueue[F[_] : Sync](customerId: String, cartBody: AddCartProduct, decodedBody: CartProduct, cartList: List[Cart], cart: Cart, customerKey: Int) =
    (
      for
        customerCart <- cartList.find(_.customerID == customerId)
        product <- customerCart.cartProducts.find(_.productID == decodedBody.productID)
      yield (product)
      ) match
      case Some(_) =>
        val productKey = cartList
          .get(customerKey)
          .get
          .cartProducts
          .map(_.productID)
          .indexOf(decodedBody.productID)

          (for
            userCart <- cartList.find(_.customerID == cart.customerID)
            userProducts = userCart.cartProducts
            product <- userProducts.find(_.productID == decodedBody.productID)
            contained = product.volumes.map(_.number).contains(cartBody.volume.number)
          yield (contained)) match
          case Some(true) =>
            cartList
              .focus(
                _.index(customerKey).cartProducts
                  .index(productKey)
                  .volumes
                  .index(
                    (for {
                      cart <- cartList.get(customerKey)
                      product <- cart.cartProducts.get(productKey)
                      volume = product.volumes.map(_.number).indexOf(cartBody.volume.number)
                    } yield (volume)).get
                  )
                  .quantity
              )
              .modify(_ + cartBody.volume.quantity)
          case _ =>
            cartList
              .focus(
                _.index(customerKey).cartProducts
                  .index(productKey)
                  .volumes
              )
              .modify(_ :+ cartBody.volume)
      case None =>
        cartList
          .focus(_.index(customerKey).cartProducts)
          .modify(
            _ :+ decodedBody
          )

  /**
    * Deletes from the cart the CartItem with a certain productID, it can be called also internally when computing the
    * orders, this behaviour is guided with the param userAction
    * @param req
    * @param customerId
    * @param repository
    * @param carts
    * @param userAction
    * @tparam F
    * @return IO[Response[IO]]
    */
  def deleteCartProd[F[_] : Sync](req: Request[IO], customerId: String, repository: RepositoryTrait[IO],
                     carts: Ref[IO, List[Cart]],
                     userAction: Boolean = true) = {
    val decodedBody: CartProductID = decode[CartProductID](parseRequestBody(req)) match
      case Right(value) => value
      case Left(error) => throw JsonParseException(s"Malformed Body ${error}")
    val result = carts
      .updateAndGet { cartList =>
        if (userAction) {
          cartList
            .find(_.customerID == customerId)
            .get
            .cartProducts
            .foreach(cartP =>
              cartP.volumes.foreach(volumeInCart =>
                repository
                  .updateMangaReservation(
                    cartP.productID,
                    volumeInCart.number,
                    0
                  )
                  .unsafeRunSync()
              )
            )
        }
        cartList.map(_.customerID).indexOf(customerId) match
          case key if key >= 0 =>
            cartList
              .focus(_.index(key).cartProducts)
              .modify(_.filter(_.productID != decodedBody.productID))
          case _ => cartList
      }
      .unsafeRunSync()

    IO(Response[IO](Status.Ok).withEntity(getUserCart(customerId, result)))
  }
package com.it.unibo.scalaapipps.services

import cats.data.Validated
import cats.effect.unsafe.implicits.global
import cats.effect.{IO, Ref, Sync}
import com.it.unibo.scalaapipps.dataValidation.*
import com.it.unibo.scalaapipps.mongo.schemas.Cart
import io.circe.Json
import io.circe.generic.*
import io.circe.parser.*
import org.bson.json.*
import org.http4s.dsl.Http4sDsl
import org.http4s.{HttpRoutes, Response, Status}
import org.http4s.Request

import io.circe.*
import io.circe.generic.auto.*
import io.circe.generic.semiauto.*
import io.circe.syntax.*
import org.http4s.circe.CirceEntityEncoder.circeEntityEncoder

object ValidateService:
  /**
    * Generic functionality made to validate Forms and similarities
    * @param req
    * @param base
    * @tparam A
    * @return IO[Response[IO]]
    */
  def validateOperation[A](req : Request[IO], base : A) : IO[Response[IO]] =
    val body = req.body
    val encodedBody = body
      .through(fs2.text.utf8.decode)
      .compile
      .string
      .unsafeRunSync()

    val result = (base match
      case _: UserDto => decode[UserDto](encodedBody) match
        case Right(value) => UserValidator.validateUser(value).fold(
          error => error.toChain.toList,
          valid => List.empty[DomainValidation]
        )
        case Left(error) => Validated.invalid(List(JsonParseException(s"Malformed Body ${error}")))
      case _: CreditCard => decode[CreditCard](encodedBody) match
        case Right(value) => CreditCardValidator.validateCreditCard(value).fold(
          error => error.toChain.toList,
          valid => List.empty[DomainValidation]
        )
        case Left(error) => Validated.invalid(List(JsonParseException(s"Malformed Body ${error}")))
      case _: Address => decode[Address](encodedBody) match
        case Right(value) => AddressValidator.validateAddress(value).fold(
          error => error.toChain.toList,
          valid => List.empty[DomainValidation]
        )
        case Left(error) => Validated.invalid(List(JsonParseException(s"Malformed Body ${error}")))
      )
    IO(Response[IO](Status.Ok).withEntity(result.toString))
package com.it.unibo.scalaapipps.mongo.schemas;

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import org.bson.types.ObjectId;


case class OrderProduct(
                         productId: String,
                         number: Integer,
                         modified: Boolean,
                         prevDispo: Integer,
                         newDispo: Integer
                       ):
  override def toString: String =
    modified match
      case true => s"The product ${productId} #${number} has been modified since " +
        s"your addition to the cart, you asked for ${prevDispo} but you can " +
        s"actually buy ${newDispo}"
      case _ => super.toString

case class OrderItem(
                      override val productID: String,
                      item: Option[VolumeToOrder],
                      override val price: Integer,
                      note: Option[String] = None
                    ) extends PricedArticle, BaseProduct

case class Order(
                  status: OrderStatus,
                  orderID: String,
                  items: List[OrderItem],
                  total: Integer = 0,
                )

case class Total(total: Integer)
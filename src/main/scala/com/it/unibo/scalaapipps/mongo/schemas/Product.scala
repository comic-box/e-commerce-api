package com.it.unibo.scalaapipps.mongo.schemas

import mongo4cats.bson.ObjectId
import com.it.unibo.scalaapipps.mongo.schemas.Volume


case class ProductNotFound(msg: String) extends NoSuchElementException(msg)

case class Product(
                    _id: ObjectId,
                    title: String,
                    description: String,
                    image: String,
                    genres: List[String],
                    status: String,
                    rating: Float,
                    authors: List[String],
                    searchID: String,
                    price: Integer, // ???
                    volumes: List[ReservedVolume]
                  )

object Product:
  val schemaName = "mangas"
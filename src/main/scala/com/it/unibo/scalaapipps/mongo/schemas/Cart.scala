package com.it.unibo.scalaapipps.mongo.schemas

import io.circe.Json

trait BaseProduct:
  val productID: String

case class CartProductID(
                          override val productID: String
                        ) extends BaseProduct

trait PricedArticle:
  val price: Integer

case class AddCartProduct(
                           override val productID: String,
                           volume: VolumeToOrder
                         ) extends BaseProduct

case class CartProduct(
                        override val productID: String,
                        override val price: Integer,
                        volumes: List[VolumeToOrder]
                      ) extends PricedArticle, BaseProduct

case class Cart(
                 customerID: String,
                 cartProducts: List[CartProduct]
               )


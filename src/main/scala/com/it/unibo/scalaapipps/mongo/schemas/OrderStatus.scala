package com.it.unibo.scalaapipps.mongo.schemas

enum OrderStatus(value: String):
  case Shipped extends OrderStatus("Shipped")
  case ShippedPartially extends OrderStatus("Partially Shipped")
  case Delivered extends OrderStatus("Delivered")
package com.it.unibo.scalaapipps.mongo.schemas

trait Volume:
  val number: Int
  val quantity: Int

case class VolumeToOrder(
                          override val number: Int,
                          override val quantity: Int
                        ) extends Volume

case class ReservedVolume(
                           override val number: Int,
                           override val quantity: Int = 0,
                           reserved: Int = 0
                         ) extends Volume

object ReservedVolume:
  def apply(unreserved: VolumeToOrder): ReservedVolume =
    ReservedVolume(unreserved.number, unreserved.quantity, 0)


case class VolumeNotFound(msg: String) extends NoSuchElementException(msg)

package com.it.unibo.scalaapipps.mongo

import cats.effect.{Async, IO, Resource}
import mongo4cats.client.*
import mongo4cats.collection.MongoCollection
import com.it.unibo.scalaapipps.mongo.schemas.{Manga, MangaNotFound, ReservedVolume, Volume, VolumeNotFound}
import org.bson.Document
import cats.effect.unsafe.implicits.global
import com.it.unibo.scalaapipps.EnvLoader
import com.mongodb.client.model.FindOneAndUpdateOptions
import com.mongodb.client.result.{InsertOneResult, UpdateResult}
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import mongo4cats.circe.*
import mongo4cats.collection.operations.Filter

import scala.concurrent.{Await, Future}
import schemas.Customer

import java.time.Instant
import scala.concurrent.duration.Duration
import scala.reflect.ClassTag
import mongo4cats.bson.ObjectId
import mongo4cats.collection.operations.Update
import com.mongodb.client.model.Filters
import mongo4cats.collection.UpdateOptions

import scala.jdk.CollectionConverters._

import scala.jdk.CollectionConverters._
import org.http4s.EntityEncoder

import io.circe.syntax._
import io.circe.generic.auto._
import cats.Applicative
import io.circe._
import io.circe.parser._

import org.http4s.EntityEncoder
import org.http4s.circe.CirceEntityCodec._
import org.http4s.circe.CirceInstances
import org.http4s.circe.*
import scala.collection.mutable.ListBuffer
import scala.util.Failure
import javax.management.InstanceAlreadyExistsException
import com.it.unibo.scalaapipps.Main.run


class DatabaseConnection() {
  val user: String = EnvLoader.getKey("MONGO_USER")
  val password: String = EnvLoader.getKey("MONGO_PASSWORD")
  val host: String = EnvLoader.getKey("MONGO_HOST")
  val port: String = EnvLoader.getKey("MONGO_PORT")
  val client = MongoClient.fromConnectionString[IO](
    s"mongodb://${user}:${password}@${host}:${port}"
  )
}

trait RepositoryTrait[IO[_]]:
  def findCustomer(customerIdentificative: ObjectId | String)(using
                                                              connection: DatabaseConnection
  ): IO[Option[Customer]]

  def getManga(title: String)(using connection: DatabaseConnection): IO[Option[Manga]]

  def getMangaWithId(title: ObjectId)(using connection: DatabaseConnection): IO[Option[Manga]]

  def getMangas()(using connection: DatabaseConnection): IO[List[Manga]]

  def updateMangaReservation(title: String, volumeNumber: Integer, reservedQty: Integer)(using
                                                                                         connection: DatabaseConnection
  ): IO[Unit] // Side Effect ^ 2

  def getVolume(id: String | ObjectId, volumeNumber: Integer)(using connection: DatabaseConnection): IO[ReservedVolume]

  def checkDispo(productId: String, volume: Volume)(using connection: DatabaseConnection): IO[Boolean]

  def updateMangaQtyWithID(pid: String, volumeNumber: Integer, qty: Integer)(using
                                                                             connection: DatabaseConnection
  ): IO[Manga]

  def incrementMangaReservationWithID(pid: String, volumeNumber: Integer)(using
                                                                          connection: DatabaseConnection
  ): IO[Unit]

  def stopService()(using connection: DatabaseConnection): IO[Unit]

object Repository:
  def impl[IO[_] : Applicative](database: String): Repository = new Repository(database)

  final case class Repository(
                               database: String
                             ) extends RepositoryTrait[IO] {

    import concurrent.ExecutionContext.Implicits.global

    def checkDispo(productId: String, volume: Volume)(using connection: DatabaseConnection): IO[Boolean] =
      this.getVolume(productId, volume.number).map(_.quantity >= volume.quantity)

    def stopService()(using connection: DatabaseConnection): IO[Unit] =
      connection.client.use { client =>
        for {
          db <- client.getDatabase(database)
          coll <- db.getCollectionWithCodec[Manga](Manga.schemaName)
          docs <- coll.updateMany(Filter.empty, Update.set("volumes.$[].reserved", 0))
        } yield docs
      }.unsafeRunSync()
      IO.unit

    def findCustomer(customer: ObjectId | String)(using connection: DatabaseConnection): IO[Option[Customer]] =
      customer match
        case customer: ObjectId =>
          connection.client.use { client =>
            for {
              db <- client.getDatabase(database)
              coll <- db.getCollectionWithCodec[Customer](Customer.schemaName)
              res <- coll.find(Filter.idEq(customer)).first
            } yield (res)
          }
        case customer: String =>
          connection.client.use { client =>
            for {
              db <- client.getDatabase(database)
              coll <- db.getCollectionWithCodec[Customer](Customer.schemaName)
              res <- coll.find(Filter.eq("CF", customer)).first
            } yield (res)
          }

    def getManga(title: String)(using connection: DatabaseConnection): IO[Option[Manga]] =
      connection.client.use { client =>
        for {
          db <- client.getDatabase(database)
          coll <- db.getCollectionWithCodec[Manga](Manga.schemaName)
          docs <- coll
            .find(Filter.eq("title", title))
            .first
        } yield docs match
          case Some(value) => Some(value)
          case None => throw MangaNotFound(s"Cannot find Product ${title}")
      }

    def getMangaWithId(id: ObjectId)(using connection: DatabaseConnection): IO[Option[Manga]] =
      connection.client.use { client =>
        for {
          db <- client.getDatabase(database)
          coll <- db.getCollectionWithCodec[Manga](Manga.schemaName)
          doc <- coll
            .find(Filter.eq("_id", id))
            .first
        } yield doc match
          case Some(value) => Some(value)
          case None => throw MangaNotFound(s"Cannot find Product ${id}")
      }

    def updateMangaReservation(title: String, volumeNumber: Integer, reservedQty: Integer)(using connection: DatabaseConnection
    ): IO[Unit] =
      val arrayFilters: java.util.List[org.bson.conversions.Bson] = List(
        Filters.eq("volume.number", volumeNumber)
      ).asJava
      connection.client.use { client =>
        for {
          db <- client.getDatabase(database)
          coll <- db.getCollectionWithCodec[Manga](Manga.schemaName)
          _ <- coll.findOneAndUpdate(
            Filter.eq("title", title),
            Update.set("volumes.$[volume].reserved", reservedQty),
            FindOneAndUpdateOptions().arrayFilters(arrayFilters)
          )
        } yield ()
      }

    def incrementMangaReservationWithID(title: String, volumeNumber: Integer)(using
                                                                              connection: DatabaseConnection
    ): IO[Unit] =
      val arrayFilters: java.util.List[org.bson.conversions.Bson] = List(
        Filters.eq("volume.number", volumeNumber)
      ).asJava
      connection.client.use { client =>
        for {
          db <- client.getDatabase(database)
          coll <- db.getCollectionWithCodec[Manga](Manga.schemaName)
          _ <- coll.findOneAndUpdate(
            Filter.eq("title", title),
            Update.inc("volumes.$[volume].reserved", 1),
            FindOneAndUpdateOptions().arrayFilters(arrayFilters)
          )
        } yield ()
      }

    def updateMangaQtyWithID(pid: String, volumeNumber: Integer, qty: Integer)(using
                                                                               connection: DatabaseConnection
    ): IO[Manga] =
      val arrayFilters: java.util.List[org.bson.conversions.Bson] = List(
        Filters.eq("volume.number", volumeNumber)
      ).asJava
      connection.client.use { client =>
        for {
          db <- client.getDatabase(database)
          coll <- db.getCollectionWithCodec[Manga](Manga.schemaName)
          doc <- coll.findOneAndUpdate(
            Filter.eq("title", pid),
            Update.set("volumes.$[volume].quantity", qty),
            FindOneAndUpdateOptions().arrayFilters(arrayFilters)
          )
        } yield doc
      }

    def getMangas()(using connection: DatabaseConnection): IO[List[Manga]] =
      connection.client.use { client =>
        for {
          db <- client.getDatabase(database)
          coll <- db.getCollectionWithCodec[Manga](Manga.schemaName)
          docs <- coll.find.stream.compile.toList
        } yield docs
      }

    def getVolume(key: String | ObjectId, volumeNumber: Integer)(using connection: DatabaseConnection): IO[ReservedVolume] =
      key match
        case key: String =>
          this
            .getManga(key)
            .map(product =>
              product.get.volumes
                .find(_.number == volumeNumber)
                .getOrElse(throw VolumeNotFound(s"Cannot find Volume number ${volumeNumber}"))
            )
        case key: ObjectId =>
          this
            .getMangaWithId(key)
            .map(product =>
              product.get.volumes
                .find(_.number == volumeNumber)
                .getOrElse(throw VolumeNotFound(s"Cannot find Volume number ${volumeNumber}"))
            )
  }

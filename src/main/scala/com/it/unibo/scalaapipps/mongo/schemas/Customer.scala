package com.it.unibo.scalaapipps.mongo.schemas

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import mongo4cats.bson.ObjectId
import mongo4cats.circe.*

object Customer:
  val schemaName: String = "customers"

  def apply(
             CF: String,
             name: String,
             surname: String,
             telegramID: String,
             email: String,
             image: String
           ): Customer = Customer(new ObjectId(), CF, name, surname, telegramID, email, image)

final case class Customer(
                           _id: ObjectId,
                           CF: String,
                           name: String,
                           surname: String,
                           telegramID: String,
                           email: String,
                           image: String
                         )

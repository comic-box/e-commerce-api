package com.it.unibo.scalaapipps.dataValidation

import cats.data.{Validated, ValidatedNec}
import com.it.unibo.scalaapipps.*

import io.circe.Json
import cats.syntax.all.*
import io.circe.generic.semiauto.deriveDecoder
import io.circe.generic.semiauto.deriveEncoder


final case class UserDto(name: String, surname: String, email: String, telephone: String)

object UserDto:
  def apply(): UserDto = UserDto("", "", "", "")
  def route = "user"

case object InvalidPhoneNumber extends DomainValidation:
  def errorMessage: String = "Invalid Italian phone number format."

case object FirstNameHasSpecialCharacters extends DomainValidation:
  def errorMessage: String = "First name cannot contain spaces, numbers or special characters."

case object LastNameHasSpecialCharacters extends DomainValidation:
  def errorMessage: String = "Last name cannot contain spaces, numbers or special characters."

case object InvalidEmail extends DomainValidation:
  def errorMessage: String = "Invalid email address."

sealed trait UserValidator:

  type ValidationResult[A] = ValidatedNec[DomainValidation, A]

  private def validatePhoneNumber(phoneNumber: String): ValidationResult[String] =
    if (phoneNumber.matches("\\+39\\d{10}"))
      phoneNumber.validNec
    else
      InvalidPhoneNumber.invalidNec

  private def validateFirstName(firstName: String): ValidationResult[String] =
    if (firstName.matches("^[a-zA-Z]+$")) firstName.validNec else FirstNameHasSpecialCharacters.invalidNec

  private def validateLastName(lastName: String): ValidationResult[String] =
    if (lastName.matches("^[a-zA-Z]+$")) lastName.validNec else LastNameHasSpecialCharacters.invalidNec

  private def validateEmail(email: String): ValidationResult[String] =
    if (email.matches("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Z|a-z]{2,6}$"))
      email.validNec
    else
      InvalidEmail.invalidNec

  def validateUser(user: UserDto): ValidationResult[UserDto] =
    (
      validateFirstName(user.name),
      validateLastName(user.surname),
      validateEmail(user.email),
      validatePhoneNumber(user.telephone),
    ).mapN((_, _, _, _) => user)


object UserValidator extends UserValidator
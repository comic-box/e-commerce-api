package com.it.unibo.scalaapipps.dataValidation

import cats.data.{Validated, ValidatedNec}
import cats.syntax.all.*
import com.it.unibo.scalaapipps.*

import io.circe.Json
import io.circe.generic.semiauto.deriveDecoder


final case class Address(user: UserDto, city: String, street: String, civic: Int, cap: Int)

object Address:
  def apply(): Address = Address(UserDto(), "", "", 0, 0)
  def route = "address"

case object InvalidCity extends DomainValidation:
  def errorMessage: String = "Invalid Italian city."


case object InvalidStreet extends DomainValidation:
  def errorMessage: String = "Invalid street name."

case object InvalidCivic extends DomainValidation:
  def errorMessage: String = "Invalid civic number."

case object InvalidCAP extends DomainValidation:
  def errorMessage: String = "Invalid CAP (postal code)."


sealed trait AddressValidator:

  type ValidationResult[A] = ValidatedNec[DomainValidation, A]

  private def validateUser(user: UserDto): ValidationResult[UserDto] =
    UserValidator.validateUser(user) match
      case Validated.Valid(a) => a.validNec
      case Validated.Invalid(e) => InvalidUser.invalidNec

  private def validateCity(city: String): ValidationResult[String] =
    if (city.matches("^[a-zA-Z]+$"))
      city.validNec
    else
      InvalidCity.invalidNec

  private def validateStreet(street: String): ValidationResult[String] =
    if (street.matches("^[a-zA-Z]+$"))
      street.validNec
    else
      InvalidStreet.invalidNec

  private def validateCivic(civic: Int): ValidationResult[Int] =
    if (civic > 0)
      civic.validNec
    else
      InvalidCivic.invalidNec

  private def validateCAP(cap: Int): ValidationResult[Int] =
    if (cap > 0)
      cap.validNec
    else
      InvalidCAP.invalidNec

  def validateAddress(address: Address): ValidationResult[Address] =
    (
      validateUser(address.user),
      validateCity(address.city),
      validateStreet(address.street),
      validateCivic(address.civic),
      validateCAP(address.cap)
    ).mapN((_, _, _, _, _) => address)

object AddressValidator extends AddressValidator
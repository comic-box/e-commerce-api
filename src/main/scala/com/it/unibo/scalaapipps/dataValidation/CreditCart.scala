package com.it.unibo.scalaapipps.dataValidation

import cats.data.{Validated, ValidatedNec}
import cats.syntax.all.*
import com.it.unibo.scalaapipps.*

import io.circe.Json

case class CreditCard(user: UserDto, number: String, expirationDate: String, cvv: String)

object CreditCard:
  def apply(): CreditCard = CreditCard(UserDto(), "", "", "")
  def route  = "credit-card"

case object InvalidCreditCardNumber extends DomainValidation:
  def errorMessage: String = "Invalid credit card number."

case object InvalidExpirationDate extends DomainValidation:
  def errorMessage: String = "Invalid expiration date format."

case object InvalidCVV extends DomainValidation:
  def errorMessage: String = "Invalid CVV format."

case object InvalidUser extends DomainValidation:
  def errorMessage: String = "Invalid User input."

sealed trait CreditCardValidator:

  type ValidationResult[A] = ValidatedNec[DomainValidation, A]

  private def validatedUser(user: UserDto): ValidationResult[UserDto] =
    UserValidator.validateUser(user) match
      case Validated.Valid(a) => a.validNec
      case Validated.Invalid(e) => InvalidUser.invalidNec

  private def validateCreditCardNumber(number: String): ValidationResult[String] =
    if (number.matches("\\d{16}"))
      number.validNec
    else
      InvalidCreditCardNumber.invalidNec

  private def validateExpirationDate(expirationDate: String): ValidationResult[String] =
    if (expirationDate.matches("\\d{2}/\\d{2}"))
      expirationDate.validNec
    else
      InvalidExpirationDate.invalidNec

  private def validateCVV(cvv: String): ValidationResult[String] =
    if (cvv.matches("\\d{3}"))
      cvv.validNec
    else
      InvalidCVV.invalidNec

  def validateCreditCard(creditCard: CreditCard): ValidationResult[CreditCard] =
    (
      validatedUser(creditCard.user),
      validateCreditCardNumber(creditCard.number),
      validateExpirationDate(creditCard.expirationDate),
      validateCVV(creditCard.cvv)
    ).mapN((_, _, _, _) => creditCard)

object CreditCardValidator extends CreditCardValidator
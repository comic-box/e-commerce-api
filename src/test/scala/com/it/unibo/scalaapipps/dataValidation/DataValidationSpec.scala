package com.it.unibo.scalaapipps.dataValidation

import cats.effect.IO
import munit.CatsEffectSuite
import org.http4s.*
import org.http4s.implicits.*

class DataValidationSpec extends CatsEffectSuite:

  test("UserValidation [ ] Case User OK") {
    val user = UserDto("matte", "santoro", "email@gmail.com", "+393481042846")
    println(
      UserValidator.validateUser(user)
    )
    assert(UserValidator.validateUser(user).isValid)
  }

  test("UserValidation [ ] Case User Wrong Email") {
    val user = UserDto("matte", "santoro", "emailgmail.com", "+393481042846")
    assert(
    UserValidator.validateUser(user).fold(
      error => error.toChain.toList,
      valid => List.empty[DomainValidation]
    )
      ==
      List(InvalidEmail))
  }

  test("UserValidation [ ] Case User Wrong Telephone") {
    val user = UserDto("matte", "santoro", "email@gmail.com", "3481042846")
    assert(
      UserValidator.validateUser(user).fold(
        error => error.toChain.toList,
        valid => List.empty[DomainValidation]
      )
        ==
        List(InvalidPhoneNumber))
  }

  test("UserValidation [ ] Case User both Wrong Telephone and Email ") {
    val user = UserDto("matte", "santoro", "emailgmail.com", "3481042846")
    assert(
      UserValidator.validateUser(user).fold(
        error => error.toChain.toList,
        valid => List.empty[DomainValidation]
      )
        ==
        List(InvalidEmail, InvalidPhoneNumber))
  }

  test("Credit card Validation [ ] Case OK") {
    val cc = CreditCard(UserDto("Matteo", "Santoro", "msantoro@gmail.com", "+391234567890"), "1234567890123456", "12/23", "123")

    assert(
      CreditCardValidator.validateCreditCard(cc).isValid
    )

  }

  test("Credit card Validation [ ] Case User for it is wrong") {
    val cc = CreditCard(UserDto("Matteo", "Santoro=", "msantoro@gmail.com", "391234567890"), "1234567890123456", "12/23", "123")

    assert(
      CreditCardValidator.validateCreditCard(cc).fold(
        error => error.toChain.toList,
        valid => List.empty[DomainValidation]
      )
        ==
        List(InvalidUser))
  }
//
//  test("bho") {
//    println(UserDto("matte", "santoro", "email@gmail.com", "+393481042846"))
//
//    println(
//      UserValidator.validateUser(
//        UserDto(
//          "John",
//          "Doe",
//          "mega@mail.com",
//          "3481042846"
//        )
//      )
//    )
//
//    println(
//      UserValidator.validateUser(
//        UserDto(
//          "John",
//          "Doe",
//          "mega@mail.com",
//          "3481042846"
//        )
//      ).fold(
//        errors => {
//          val errorMessages = errors.toChain.toList.map(_.errorMessage)
//          s"Validation failed with errors: ${errorMessages.mkString(", ")}"
//        },
//        // Funzione chiamata in caso di successo
//        userDto => s"Validation successful: $userDto"
//      )
//    )
//
//    println(
//      UserValidator.validateUser(
//        UserDto(
//          "John",
//          "Doe",
//          "mega@mail.com",
//          "+393481042846"
//        )
//      )
//    )
//
//    println(
//      UserValidator.validateUser(
//        UserDto(
//          "John",
//          "Doe!#",
//          "mega@mail.com",
//          "+393481042846"
//        )
//      ).toEither
//    )
//  }


//  test("HelloWorld returns status code 200") {
//    assertIO(retHelloWorld.map(_.status) ,Status.Ok)
//  }
//
//  test("HelloWorld returns hello world message") {
//    assertIO(retHelloWorld.flatMap(_.as[String]), "{\"message\":\"Hello, world\"}")
//  }
//
//  private[this] val retHelloWorld: IO[Response[IO]] =
//    val getHW = Request[IO](Method.GET, uri"/hello/world")
//    val helloWorld = HelloWorld.impl[IO]
//    ScalaapippsRoutes.helloWorldRoutes(helloWorld).orNotFound(getHW)

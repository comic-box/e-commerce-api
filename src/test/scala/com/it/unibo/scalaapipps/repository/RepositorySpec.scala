package com.it.unibo.scalaapipps.repository

import cats.effect.IO
import com.it.unibo.scalaapipps.EnvLoader
import munit.CatsEffectSuite
import org.http4s.*
import org.http4s.implicits.*
import com.it.unibo.scalaapipps.mongo.Repository
import com.it.unibo.scalaapipps.mongo.schemas.{Customer, MangaNotFound, VolumeNotFound}
import com.mongodb.client.result.InsertOneResult
import munit.Clue.generate

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import mongo4cats.bson.ObjectId
import com.it.unibo.scalaapipps.mongo.DatabaseConnection
import io.circe.literal.json
import org.junit.Assert.assertThrows

class RepositorySpec extends CatsEffectSuite:
  private[this] val rep = Repository.impl[IO](EnvLoader.getKey("MONGO_DB_NAME"))
  private[this] val callOfTheNight= "Yofukashi no uta"
  private[this] val nagatoro = "Please don\\'t bully me, Nagatoro"
  private[this] val silvioID= "slvBrl"

  override def afterEach(context: AfterEach): Unit =
    rep.stopService()
  given DatabaseConnection: DatabaseConnection = new DatabaseConnection()

  test("Should get Call of the night") {
    assertIO(rep.getManga(callOfTheNight).map(_.get.searchID), "yofukashi_no_uta")
  }

  test("Should throw") {
    interceptIO[MangaNotFound](rep.getManga("Inuyasha"))
  }

  test("Should get Call of the night by ID".ignore) {
    assertIO(rep.getMangaWithId(ObjectId("64a80e5c3de1f26f2d634e84")).map(_.get.searchID), "yofukashi_no_uta")
  }

  test("Should get CallOfTheNight volume 132".ignore) {
    assertIO(rep.getVolume(ObjectId("64a80e5c3de1f26f2d634e84"), 132).map(_.number), 132)
  }

  test("Should get Nagatoro volume 179") {
//    assertIO(rep.getVolume("Please don\\'t bully me, Nagatoro", 179).map(_.number), 179)
    assertIO(rep.getVolume(callOfTheNight, 179).map(_.number), 179)
  }

  test("Should not get CallOfTheNight volume and thorw") {
    interceptIO[VolumeNotFound](rep.getVolume(callOfTheNight, 999))
  }

  test("Should update Call of the night") {
    rep.updateMangaReservation(callOfTheNight, 183, 12).unsafeRunSync()
    assertIO(rep.getVolume(callOfTheNight, 183).map(_.reserved), 12)
    rep.updateMangaReservation(callOfTheNight, 183, 0).unsafeRunSync()
  }

  test("Should find customer".ignore) {
    val silvioID: ObjectId = ObjectId("64af02200bec0b0c44f9dcc3")
    assertIO(rep.findCustomer(silvioID).map(_.get.name), "silvio")
  }

  test("Should find customer withCF") {
    assertIO(rep.findCustomer(silvioID).map(_.get.name), "silvio")
  }


  test("Should update Call of the night by ID") {
    rep.incrementMangaReservationWithID(callOfTheNight, 183).unsafeRunSync()
    assertIO(rep.getManga(callOfTheNight).map(_.get.volumes.find(_.number == 183).get.reserved), 1)
  }
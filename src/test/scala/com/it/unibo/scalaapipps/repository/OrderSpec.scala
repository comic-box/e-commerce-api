package com.it.unibo.scalaapipps.repository

import cats.effect.IO.asyncForIO
import cats.effect.{IO, Ref, Sync}
import com.it.unibo.scalaapipps.EnvLoader
import com.it.unibo.scalaapipps.mongo.{DatabaseConnection, Repository}
import com.it.unibo.scalaapipps.mongo.schemas.{AddCartProduct, Cart, CartProduct, Customer, Order, OrderItem, ReservedVolume, Volume, VolumeToOrder}
import com.it.unibo.scalaapipps.routes.{CartRoutes, OrderRoutes}
import com.mongodb.client.result.{InsertOneResult, UpdateResult}
import io.circe.Json
import io.circe.literal.json
import mongo4cats.bson.ObjectId
import munit.CatsEffectSuite
import munit.Clue.generate
import munit.Tag
import org.http4s
import org.http4s.*
import org.http4s.implicits.*
import io.circe.*
import io.circe.parser.*

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import io.circe.generic.auto.*
import io.circe.syntax.*
import munit.Clue.generate
import org.http4s.circe.CirceEntityEncoder.*

//testOnly OrderSpec -- -l com.it.unibo.tags.EdgeTest
class OrderSpec extends CatsEffectSuite:
  given DatabaseConnection: DatabaseConnection = new DatabaseConnection()
  private[this] val rep = Repository.impl[IO](EnvLoader.getKey("MONGO_DB_NAME"))
  private[this] val userCarts: Ref[IO, List[Cart]] =
    Ref.unsafe[IO, List[Cart]](List.empty)
  //    private[this] val serverOrders: Map[String, Ref[IO, Order]] = Map()
  private[this] val serverOrders: Ref[IO, Map[String, List[Order]]] =
    Ref.unsafe[IO, Map[String, List[Order]]](Map())
//  private[this] val nagatoro = "Please don\\'t bully me, Nagatoro"
  private[this] val callOfTheNight = "Yofukashi no uta"
  private[this] val stdPrice = 6
  import math.Fractional.Implicits.infixFractionalOps
  import math.Integral.Implicits.infixIntegralOps
  import math.Numeric.Implicits.infixNumericOps
  import munit.Clue.generate

  object EdgeTest extends Tag("com.it.unibo.tags.EdgeTest")

  override def afterEach(context: AfterEach): Unit =
//    userCarts.clear()
    rep.stopService()
    userCarts.update { _ =>
      List.empty[Cart]
    }
    serverOrders.update { _ =>
      Map()
    }
    super.afterEach(context)

//  test("Should add order.....") {
//    val silvioID = ObjectId("64af02200bec0b0c44f9dcc3")
//    val nagatoro = new ObjectId("64a80e5c3de1f26f2d634e82")
//    val centoventitre = Volume(123, 1)
//    val callToAdd =
//      json"""{"productId" : ${nagatoro.toString}, "volumeNumber" : ${centoventitre.number}, " voulmeQty" :  ${centoventitre.quantity}}"""
//    val addProduct = Request[IO](Method.POST, uri"/cart/64af02200bec0b0c44f9dcc3")
//     CartRoutes.addProduct(rep, userCarts).orNotFound(addProduct.withEntity(callToAdd)).unsafeRunSync()
////    val manga = rep.getMangaWithId(nagatoro.toString).unsafeRunSync().get
//    val mangas = rep.getMangas().unsafeRunSync()
////    val bho = userCarts.get(silvioID.toString).get.get.map{
////      cart => Order(new ObjectId(), cart.cartProducts.flatMap{cProd => cProd.volumes.map{
////        volume => OrderItem(volume, mangas.find(doc => doc.containsValue(nagatoro)).get.get("price").toString.toFloat)
////      }})
////    }.unsafeRunSync()
////    println(bho)
//  }

  test("Should check dispo in stock with cart [ ] Case : can Buy") {
    val silvioID = "slvBrl"
    val centotrentanove = VolumeToOrder(139, 1)
    val callToAdd = AddCartProduct(callOfTheNight, centotrentanove).asJson
    val addProduct = Request[IO](Method.POST, uri"/cart"./(silvioID))
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAdd))
      .unsafeRunSync()
    assertIO(rep.checkDispo(callOfTheNight, centotrentanove), true)
  }

  /**
    * If it fails check the DB the dispo must be 0 !
    */
  test("Should check dispo in stock with cart [ ] Case : can not Buy".ignore) {
    val silvioID = "slvBrl"
    val centotrentuno = VolumeToOrder(131, 1)
    val callToAdd = AddCartProduct(callOfTheNight, centotrentuno).asJson
    val addProduct = Request[IO](Method.POST, uri"/cart"./(silvioID))
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAdd))
      .unsafeRunSync()
    assertIO(rep.checkDispo(callOfTheNight, centotrentuno), false)
  }

  /** This test has to be run on his own
    */
  test("Should process cart [ ] Base case : OK".ignore) {
    val silvioID = "slvBrl"
    val centotrentadue = VolumeToOrder(132, 1)
    val volumeBefore = rep.getVolume(callOfTheNight, 132).unsafeRunSync()
    val callToAdd = AddCartProduct(callOfTheNight, centotrentadue).asJson
    val addProduct = Request[IO](Method.POST, uri"/cart"./(silvioID))
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAdd))
      .unsafeRunSync()
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAdd))
      .unsafeRunSync()

    val processCart = Request[IO](Method.POST, uri"/order"./(silvioID))
    OrderRoutes
      .processOrderController(rep, userCarts, serverOrders)
      .orNotFound(processCart)
      .unsafeRunSync()
    assertIO(
      rep.getVolume(callOfTheNight, 132),
      ReservedVolume(132, volumeBefore.quantity - 2, 0)
    )
  }

  /** It s ok for this test to fail, edge case on an almost empty qty on the DB
    */
  test(
    "Should process cart [ ] case : Problem with a product 2 item in cart and only 1 in stock".ignore
  ) {
    val silvioID = "slvBrl"
    val centoottantadue = VolumeToOrder(182, 1)

    val volumeBefore = rep.getVolume(callOfTheNight, 182).unsafeRunSync()
    val callToAdd = AddCartProduct(callOfTheNight, centoottantadue).asJson
    val addProduct = Request[IO](Method.POST, uri"/cart"./(silvioID))
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAdd))
      .unsafeRunSync()
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAdd))
      .unsafeRunSync()
//    OrderRoutes.processOrder(rep, silvioID.toString, userCarts, serverOrders)
    val processCart = Request[IO](Method.POST, uri"/order"./(silvioID))
    OrderRoutes
      .processOrderController(rep, userCarts, serverOrders)
      .orNotFound(processCart)
      .unsafeRunSync()

    println(
      serverOrders.get.unsafeRunSync()
    )
    assertIO(
      rep.getVolume(callOfTheNight, 182),
      ReservedVolume(182, volumeBefore.quantity - 1, 0)
    )
  }

  /** It s ok for this test to fail, edge case on an almost empty qty on the DB
    */
  test(
    "Should process cart [ ] case : 2 users buy the same product and one it s over".ignore
  ) {
    val silvioID = "slvBrl"
    val matteID = "mttsnt"
    val centoottantadue = VolumeToOrder(182, 1)

    val volumeBefore = rep.getVolume(callOfTheNight, 182).unsafeRunSync()
    val callToAdd = AddCartProduct(callOfTheNight, centoottantadue).asJson
    val addProductSilvio = Request[IO](Method.POST, uri"/cart"./(silvioID))
    val addProductMatte = Request[IO](Method.POST, uri"/cart"./(matteID))
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProductSilvio.withEntity(callToAdd))
      .unsafeRunSync()
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProductMatte.withEntity(callToAdd))
      .unsafeRunSync()
    //    OrderRoutes.processOrder(rep, silvioID.toString, userCarts, serverOrders)
    val processCart = Request[IO](Method.POST, uri"/order"./(silvioID))
    OrderRoutes
      .processOrderController(rep, userCarts, serverOrders)
      .orNotFound(processCart)
      .unsafeRunSync()

    val processCartMatte = Request[IO](Method.POST, uri"/order"./(matteID))
    OrderRoutes
      .processOrderController(rep, userCarts, serverOrders)
      .orNotFound(processCartMatte)
      .unsafeRunSync()

    println(
      serverOrders.get.unsafeRunSync()
    )
    assertIO(
      rep.getVolume(callOfTheNight, 182),
      ReservedVolume(182, volumeBefore.quantity - 1, 0)
    )
  }


//<<<<<<< HEAD
//  private[this] val serverEmualtor: IO[Response[IO]] =
//    val repo = Repository.impl[IO](databaseTest)
//    val body = json"""{"hello":"world"}"""
//    val userCarts: Map[String, Ref[IO, Cart]] = Map()
//    val addProduct = Request[IO](Method.POST, uri"/product")
//    import io.circe.syntax._
//    import io.circe.generic.auto._
//    import org.http4s.circe.CirceEntityEncoder._
//    ScalaapippsRoutes.addProduct(repo, userCarts).orNotFound(addProduct.withEntity(body))
//=======
//  private[this] val serverEmualtor: IO[Response[IO]] =
//    val repo = Repository.impl[IO](databaseTest)
//    val userCarts: Map[String, Ref[IO, Cart]] = Map()
//    val addProduct = Request[IO](Method.GET, uri"/product")
//    ScalaapippsRoutes.addProduct(repo, userCarts).orNotFound(addProduct)
//>>>>>>> c9f7b12984e53e621ebd8a641cdf607b1e08aa6a

//      .orNotFound(addProduct)
//    val getHW = Request[IO](Method.GET, uri"/hello/world")
//    val addProductRoute = ScalaapippsRoutes.addProduct[F](rep, userCarts)
//    ScalaapippsRoutes.addProduct[](rep, userCarts)

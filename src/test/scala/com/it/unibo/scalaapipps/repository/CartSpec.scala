package com.it.unibo.scalaapipps.repository

import cats.effect.{IO, Ref, Sync}
import com.it.unibo.scalaapipps.EnvLoader
import com.it.unibo.scalaapipps.mongo.schemas.{AddCartProduct, Cart, CartProduct, CartProductID, Customer, ReservedVolume, Total, Volume, VolumeNotFound, VolumeToOrder}
import com.mongodb.client.result.{InsertOneResult, UpdateResult}
import munit.CatsEffectSuite
import munit.Clue.generate
import org.http4s.*
import org.http4s.implicits.*

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import com.it.unibo.scalaapipps.mongo.Repository
import com.it.unibo.scalaapipps.mongo.DatabaseConnection
import cats.effect.IO.asyncForIO
import com.it.unibo.scalaapipps.routes.CartRoutes
import io.circe.Json
import io.circe.literal.json
import io.circe.*
import io.circe.parser.*
import mongo4cats.bson.ObjectId
import org.http4s
//import cats.effect.SyncIO.syncForSyncIO
import munit.Clue.generate
import io.circe.syntax._
import io.circe.generic.auto._
import org.http4s.circe.CirceEntityEncoder._
class CartSpec extends CatsEffectSuite:
  given DatabaseConnection: DatabaseConnection = new DatabaseConnection()
  private[this] val rep = Repository.impl[IO](EnvLoader.getKey("MONGO_DB_NAME"))
  private[this] val userCarts: Ref[IO, List[Cart]] =
    Ref.unsafe[IO, List[Cart]](List.empty)
  private[this] val dangerInMYHeart = "Boku no Kokoro no Yabai yatsu"
  private[this] val callOfTheNight = "Yofukashi no uta"
  private[this] val silvioID = "slvBrl"
  override def afterAll(): Unit =
    println("Cleaning the DB")
    // Clean all the dirty reservations into the db
    rep.stopService().unsafeRunSync()
    super.afterAll()
  override def afterEach(context: AfterEach): Unit =
    userCarts
      .update { _ =>
        List.empty[Cart]
      }
      .unsafeRunSync()
    super.afterEach(context)
  test("Should add product to Cart [ ] Base Case: Empty Cart Ref") {
    val volumeAdded = VolumeToOrder(123, 1)
    val stdPrice = rep.getManga(callOfTheNight).unsafeRunSync().get.price
    val callToAdd = AddCartProduct(callOfTheNight, volumeAdded).asJson
    val response = Cart(silvioID, CartProduct(callOfTheNight, stdPrice, volumeAdded :: Nil) :: Nil).asJson
    val addProduct = Request[IO](Method.POST, uri"/cart"./(silvioID))
    val serverEmualtor = CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAdd))
    assertIO(
      serverEmualtor
        .flatMap(_.as[String])
        .map(x => parse(x).getOrElse(Json.Null)),
      response
    )
  }

  test("Should throw [ ] Case: Volume Not Found") {
    val throwVol = VolumeToOrder(999, 1)
    val callToAdd = AddCartProduct(callOfTheNight, throwVol).asJson
    val addProduct = Request[IO](Method.POST, uri"/cart"./(silvioID))
    assertIO(
      CartRoutes
        .addProductController(rep, userCarts)
        .orNotFound(addProduct.withEntity(callToAdd))
        .map(_.body.through(fs2.text.utf8.decode)
          .compile
          .string.unsafeRunSync()),
      "\"com.it.unibo.scalaapipps.mongo.schemas.VolumeNotFound: Cannot find Volume number 999\""
    )
  }

  test("Should throw [ ] Case: Prod Not Found") {
    val vol = VolumeToOrder(1, 1)
    val callToAdd = AddCartProduct(callOfTheNight + ".....", vol).asJson
    val addProduct = Request[IO](Method.POST, uri"/cart"./(silvioID))
    assertIO(
      CartRoutes
        .addProductController(rep, userCarts)
        .orNotFound(addProduct.withEntity(callToAdd))
        .map(_.body.through(fs2.text.utf8.decode)
          .compile
          .string.unsafeRunSync()),
      "\"com.it.unibo.scalaapipps.mongo.schemas.MangaNotFound: Cannot find Product Yofukashi no uta.....\""
    )
  }

  test("Should add product to Cart [ ] Case: Non-Empty Cart Ref") {
    val firstVolumeAdded = VolumeToOrder(123, 1)
    val secondVolumeAdded = VolumeToOrder(124, 1)
    val stdPrice = rep.getManga(dangerInMYHeart).unsafeRunSync().get.price
    val callToAdd = AddCartProduct(dangerInMYHeart, firstVolumeAdded).asJson
    val addProduct = Request[IO](Method.POST, uri"/cart"./(silvioID))
    val serverEmualtor = CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAdd))
    serverEmualtor.unsafeRunSync()

    val secondCallToAdd = AddCartProduct(dangerInMYHeart, secondVolumeAdded).asJson
    val response = Cart(silvioID, CartProduct(dangerInMYHeart, stdPrice, firstVolumeAdded :: secondVolumeAdded :: Nil) :: Nil).asJson
    val serverEmualtor2 = CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(secondCallToAdd))
    assertIO(
      serverEmualtor2
        .flatMap(_.as[String])
        .map(x => parse(x).getOrElse(Json.Null)),
      response
    )
//    assertIO(serverEmualtor2.flatMap(_.as[String]), response.toString.filter(!_.isWhitespace))
  }

  test("Should add product to Cart [ ] Case: 2 different Products") {
    val callOfTheNightVol = VolumeToOrder(133, 1)
    val stdPriceCOFTN = rep.getManga(callOfTheNight).unsafeRunSync().get.price
    val stdPriceDIMH = rep.getManga(dangerInMYHeart).unsafeRunSync().get.price
    val dangerIMHVol = VolumeToOrder(126, 1)
    val response = Cart(silvioID,
      CartProduct(callOfTheNight, stdPriceCOFTN, callOfTheNightVol :: Nil) ::
        CartProduct(dangerInMYHeart, stdPriceDIMH, dangerIMHVol :: Nil) :: Nil).asJson
    val callToAddNagatoro = AddCartProduct(callOfTheNight, callOfTheNightVol).asJson
    val callToAddDangerIMH = AddCartProduct(dangerInMYHeart, dangerIMHVol).asJson

    val addProduct = Request[IO](Method.POST, uri"/cart"./(silvioID))
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAddNagatoro))
      .unsafeRunSync()
    assertIO(
      CartRoutes
        .addProductController(rep, userCarts)
        .orNotFound(addProduct.withEntity(callToAddDangerIMH))
        .flatMap(_.as[String])
        .map(x => parse(x).getOrElse(Json.Null)),
      response
    )
  }

  test(
    "Should add product to Cart [ ] Case: Non-Empty Cart Ref and same Product so increase qty"
  ) {
    val volumeAdded = VolumeToOrder(123, 1)
    val responseVolume = VolumeToOrder(123, 2)
    val stdPrice = rep.getManga(dangerInMYHeart).unsafeRunSync().get.price
    val response = Cart(silvioID,
      CartProduct(dangerInMYHeart, stdPrice, responseVolume :: Nil) :: Nil).asJson
    val callToAdd = AddCartProduct(dangerInMYHeart, volumeAdded).asJson
    val addProduct = Request[IO](Method.POST, uri"/cart"./(silvioID))
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAdd))
      .unsafeRunSync()
    assertIO(
      CartRoutes
        .addProductController(rep, userCarts)
        .orNotFound(addProduct.withEntity(callToAdd))
        .flatMap(_.as[String])
        .map(x => parse(x).getOrElse(Json.Null)),
      response
    )
  }

  test("Should remove product to existing Cart [ ] Case: Non-Empty Cart Ref") {
    val volumeAdded = VolumeToOrder(123, 1)
    val callToAdd = AddCartProduct(dangerInMYHeart, volumeAdded)

    val addProduct = Request[IO](Method.POST, uri"/cart"./(silvioID))
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAdd))
      .unsafeRunSync()


    val callToRemove = CartProductID(dangerInMYHeart)

    val response = Cart(silvioID, List.empty[CartProduct]).asJson
    val removeProduct = Request[IO](Method.DELETE, uri"/cart"./(silvioID))
    val serverEmualtor = CartRoutes
      .removeCartProductController(rep, userCarts)
      .orNotFound(removeProduct.withEntity(callToRemove))
    assertIO(
      serverEmualtor
        .flatMap(_.as[String])
        .map(x => parse(x).getOrElse(Json.Null)),
      response
    )
  }

  test(
    "Should remove product to existing Cart [ ] Case: Non-Empty Cart Ref And check reserved qty is OK"
  ) {
    val volumeAdded = VolumeToOrder(123, 1)

    val callToAdd = AddCartProduct(dangerInMYHeart, volumeAdded).asJson

    val addProduct = Request[IO](Method.POST, uri"/cart"./(silvioID))
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAdd))
      .unsafeRunSync()

    val callToRemove = CartProductID(dangerInMYHeart)
    val removeProduct = Request[IO](Method.DELETE, uri"/cart"./(silvioID))
    CartRoutes
      .removeCartProductController(rep, userCarts)
      .orNotFound(removeProduct.withEntity(callToRemove))
      .unsafeRunSync()
    assertIO(
      rep.getVolume(dangerInMYHeart, volumeAdded.number).map(_.reserved),
      0
    )
    // clean teh response string cause is faster to write tests
//    assertIO(serverEmualtor.flatMap(_.as[String]), response.toString.filter(!_.isWhitespace))
  }

  test("Should not remove product to existing Cart [ ] Case: Different User") {
    val user2 = "mttSnt"
    val volumeAdded = VolumeToOrder(123, 1)
    val callToAdd = AddCartProduct(dangerInMYHeart, volumeAdded).asJson
    val stdPrice = rep.getManga(dangerInMYHeart).unsafeRunSync().get.price

    val addProduct = Request[IO](Method.POST, uri"/cart"./(silvioID))
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAdd))
      .unsafeRunSync()

    val addProductuser2 = Request[IO](Method.POST, uri"/cart"./(user2))
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProductuser2.withEntity(callToAdd))
      .unsafeRunSync()

    val callToRemove = CartProductID(dangerInMYHeart)
    val response = Cart(silvioID, CartProduct(dangerInMYHeart, stdPrice, volumeAdded :: Nil) :: Nil).asJson

    val removeProduct = Request[IO](Method.DELETE, uri"/cart"./(user2))
    CartRoutes
      .removeCartProductController(rep, userCarts)
      .orNotFound(removeProduct.withEntity(callToRemove))
      .unsafeRunSync()

    val getCarts = Request[IO](Method.GET, uri"/cart"./(silvioID))
    val serverEmulator = CartRoutes.getCartController(userCarts).orNotFound(getCarts)
    assertIO(
      serverEmulator
        .flatMap(_.as[String])
        .map(x => parse(x).getOrElse(Json.Null)),
      response
    )
  }

  test("Should add to cart Product, remove it and then add without errors") {
    val volumeAdded = VolumeToOrder(123, 1)
    val callToAdd = AddCartProduct(dangerInMYHeart, volumeAdded).asJson
    val stdPrice = rep.getManga(dangerInMYHeart).unsafeRunSync().get.price

    val response = Cart(silvioID, CartProduct(dangerInMYHeart, stdPrice, volumeAdded :: Nil) :: Nil).asJson
    val addProduct = Request[IO](Method.POST, uri"/cart"./(silvioID))
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAdd))
      .unsafeRunSync()
    val callToRemove = CartProductID(dangerInMYHeart)

    val removeProduct = Request[IO](Method.DELETE, uri"/cart"./(silvioID))
    CartRoutes
      .removeCartProductController(rep, userCarts)
      .orNotFound(removeProduct.withEntity(callToRemove))
      .unsafeRunSync()

    val serverEmulator = CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAdd))
    assertIO(
      serverEmulator
        .flatMap(_.as[String])
        .map(x => parse(x).getOrElse(Json.Null)),
      response
    )
  }

  test("Should get Cart [ ] Full Cart") {
    val firstVolumeAdded = VolumeToOrder(123, 1)
    val secondVolumeAdded = VolumeToOrder(124, 1)
    val callToAdd = AddCartProduct(dangerInMYHeart,firstVolumeAdded).asJson
    val stdPrice = rep.getManga(dangerInMYHeart).unsafeRunSync().get.price
    val response = Cart(silvioID, CartProduct(dangerInMYHeart, stdPrice, firstVolumeAdded :: secondVolumeAdded :: Nil) :: Nil).asJson

    val addProduct = Request[IO](Method.POST, uri"/cart"./(silvioID))
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAdd))
      .unsafeRunSync()
    val secondCallToAdd  = AddCartProduct(dangerInMYHeart, secondVolumeAdded).asJson
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(secondCallToAdd))
      .unsafeRunSync()

    val getCarts = Request[IO](Method.GET, uri"/cart"./(silvioID))
    val serverEmulator = CartRoutes.getCartController(userCarts).orNotFound(getCarts)
    assertIO(
      serverEmulator
        .flatMap(_.as[String])
        .map(x => parse(x).getOrElse(Json.Null)),
      response
    )
  }

  test("Should get Cart [ ] Empty Cart") {
    val response = Cart(silvioID, List.empty[CartProduct]).asJson
    val getCarts = Request[IO](Method.GET, uri"/cart"./(silvioID))
    val serverEmulator = CartRoutes.getCartController(userCarts).orNotFound(getCarts)
    assertIO(
      serverEmulator
        .flatMap(_.as[String])
        .map(x => parse(x).getOrElse(Json.Null)),
      response
    )
//    assertIO(serverEmulator.flatMap(_.as[String]), response.toString.filter(!_.isWhitespace))
  }

  test(
    "Should add product to existing Cart and add reserved qty to product [ ] Case base"
  ) {
    val volumeAdded = VolumeToOrder(123, 1)
    val callToAdd = AddCartProduct(dangerInMYHeart, volumeAdded).asJson

    val addProduct = Request[IO](Method.POST, uri"/cart"./(silvioID))
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAdd))
      .unsafeRunSync()
    assertIO(
      rep
        .getManga(dangerInMYHeart)
        .map(_.get.volumes.find(_.number == volumeAdded.number).get.quantity),
      1
    )
    // Keep the DB clean
    rep
      .updateMangaReservation(dangerInMYHeart, volumeAdded.number, 0)
      .unsafeRunSync()
//
  }

  test("Should get total cost of Cart") {
    val priceCall = rep.getManga(callOfTheNight).unsafeRunSync().map(_.price).get
    val priceDangerIMH =
      rep.getManga(dangerInMYHeart).unsafeRunSync().map(_.price).get
    val totalExpected = priceCall + priceDangerIMH
    val callOfTheNightVol = VolumeToOrder(133, 1)
    val dangerIMHVol = VolumeToOrder(126, 1)
    val response = Total(totalExpected).asJson
    val callToAddCallOfTheNight = AddCartProduct(callOfTheNight, callOfTheNightVol )

    val callToAddDangerIMH = AddCartProduct(dangerInMYHeart, dangerIMHVol)
    val addProduct = Request[IO](Method.POST, uri"/cart"./(silvioID))
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAddCallOfTheNight))
      .unsafeRunSync()
    CartRoutes
      .addProductController(rep, userCarts)
      .orNotFound(addProduct.withEntity(callToAddDangerIMH))
      .unsafeRunSync()
    val getTotal = Request[IO](Method.GET, uri"/cart/total"./(silvioID))
    val serverEmulator = CartRoutes.getTotalController(userCarts).orNotFound(getTotal)
    assertIO(
      serverEmulator
        .flatMap(_.as[String])
        .map(x => parse(x).getOrElse(Json.Null)),
      response
    )
  }

FROM hseeberger/scala-sbt:11.0.11_1.5.5_2.13.6

WORKDIR /app

COPY build.sbt ./
COPY .env ./
COPY src ./src

RUN sbt compile

EXPOSE 1040

CMD sbt run